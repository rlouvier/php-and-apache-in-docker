# Run composer install from laravel app dir and copy artifact into php apache image. This is easier than using nginx, in my opinion.
# You can change the build directory/versions as needed.
# This would be a great docker image for use with Kubernetes
# Note: You can also run native docker-php-ext in the below container. A run for configuring/install intl extension  might look like this -
#  RUN apt-get update && apt-get install -y \
#        zlib1g-dev \
#        libicu-dev \
#        g++ \
#    && docker-php-ext-configure intl \
#    && docker-php-ext-install intl

FROM composer:1.8.6 as build
MAINTAINER rlouvier@remax.com
WORKDIR /app
COPY . /app
RUN composer install

# Copy vhost file from code directory. Change the port by updating it here and the vhost.conf
FROM php:7.4-apache
EXPOSE 80
COPY --from=build /app /app
COPY vhost.conf /etc/apache2/sites-available/000-default.conf
RUN chown -R www-data:www-data /app \
  && a2enmod rewrite
